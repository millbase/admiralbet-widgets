pipeline {
    agent {
        label 'AdmiralSlave'
    }

    tools {
        git 'Default'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        skipDefaultCheckout()
        ansiColor('xterm')
        disableConcurrentBuilds()
        timestamps()
    }
    
    stages {
        
        stage('Clean & Checkout') {
            steps {
                sh 'echo branch: $branch'
                notifyBuild('STARTED', '')
                deleteDir()
                cleanWs()
                git branch: 'develop', credentialsId: 'b1e37515-2e72-4f2e-9ccb-b44e34168ce6', url: 'https://cimbet@bitbucket.org/millbase/admiralbet-widgets.git'
            }
        }
        
        stage('Prpr') {
            steps {
                nodejs('NodeJS 14.17.3') {
                    sh 'node --version && npm --version && npm install && ${WORKSPACE}/node_modules/@angular/cli/bin/ng.js --version'
                }
            }
        }

        stage('Build') {
            steps {
                nodejs('NodeJS 14.17.3') {
                    sh '''
                    ${WORKSPACE}/node_modules/@angular/cli/bin/ng.js build --configuration=universal_stage && \
                    cd ${WORKSPACE} && \
                    cat dist/angular-web-component/runtime.*.js dist/angular-web-component/polyfills.*.js dist/angular-web-component/main.*.js > dist/admiral-web-component.js
                    '''
                }
            }
        }
        
        stage('Dply') {
            steps {
                sh 'set +x && sudo -u ansible /home/ansible/bin/widget/deploy-stage.sh "${WORKSPACE}/dist" admiral-web-component.js && set -x'
            }
        }
    }
    
    post { 
        success { 
            notifyBuild("SUCCESSFUL", currentBuild.durationString)
        }
        failure {
            notifyBuild(currentBuild.result, currentBuild.durationString)
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED', String durationString = '') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${durationString}'".replaceAll(' and counting', "")
  def summary = "${subject} (<${env.BUILD_URL}|Open>)"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary, channel: '#jenkins', tokenCredentialId: 'slack-token', iconEmoji: 'https://a.slack-edge.com/80588/img/services/jenkins-ci_72.png')
}
