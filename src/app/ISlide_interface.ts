import {IImage} from "./IImage_interface";

export interface ISlide {
  image: IImage;
  action: string;
  leftSide: boolean;
  rightSide: boolean;
  selected: boolean;
  loaded: boolean;
}
