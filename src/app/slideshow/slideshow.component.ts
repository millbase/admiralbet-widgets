import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  Output,
  PLATFORM_ID,
  Renderer2,
  ViewChild,
  DoCheck,
  NgZone,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
} from '@angular/core';
import { isPlatformServer, DOCUMENT } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DomSanitizer, TransferState, makeStateKey, SafeStyle } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { SlideshowService } from './slideshow.service';
import {IImage} from "../IImage_interface";
import { environment } from 'src/environments/environment';
import {ISlide} from "../ISlide_interface";
import {HelpersService} from "../services/helpers.service";
import {GetDataService} from "../services/get-data.service";

const FIRST_SLIDE_KEY = makeStateKey<any>('firstSlide');

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.scss'],
  providers: [SlideshowService, TransferState],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideIn', [
      state(
        'outOfView',
        style({
          transform: 'translateX(100%)',
          opacity: 0,
        })
      ),
      state(
        'slideInView',
        style({
          transform: 'translateX(0)',
          opacity: 1,
        })
      ),
      transition('outOfView => slideInView', [animate('1s')]),
      transition('slideInView => outOfView', [animate('10ms 500ms')]),
    ]),
  ],
})
export class NewSlideshowComponent implements OnInit, AfterViewInit, DoCheck, OnChanges, OnDestroy {
  public slideIndex: number = -1;
  public slides: any[] = [];
  public hideLeftArrow: boolean = false;
  public hideRightArrow: boolean = false;
  public isMobile = this.helpersService.isMobile();
  private urlCache: (string | IImage)[] ;
  private autoplayIntervalId: any;
  private initial: boolean = true;
  private isHidden: boolean = false;
  private slideSub: Subscription;
  private clickSub: Subscription;
  public environment = environment;
  public minPromoGameImageWidth = this.isMobile ? '31.5%' : '33.33%';
  public dots = [];

  // Property Binding Options
  // @ts-ignore
  @Input() imageUrls: (string | IImage)[];
  @Input() buttonPosition: string = 'left';
  @Input() height: string = '100%';
  @Input() minHeight: string;
  @Input() arrowSize: string;
  @Input() showArrows: boolean = true;
  @Input() disableSwiping: boolean = false;
  @Input() autoPlay: boolean = false;
  @Input() autoPlayInterval: number = 3333;
  @Input() stopAutoPlayOnSlide: boolean = true;
  @Input() autoPlayWaitForLazyLoad: boolean = true;
  @Input() backgroundSize: string = 'cover';
  @Input() backgroundPosition: string = 'center center';
  @Input() backgroundRepeat: string = 'no-repeat';
  @Input() showDots: boolean = false;
  @Input() dotColor: string = '#FFF';
  @Input() showCaptions: boolean = true;
  @Input() captionColor: string = '#FFF';
  @Input() lazyLoad: boolean = false;
  @Input() hideOnNoSlides: boolean = false;
  @Input() fullscreen: boolean = false;
  @Input() enableZoom: boolean = false;
  @Input() enablePan: boolean = false;
  @Input() noLoop: boolean = false;
  @Input() isButtonAllowed: boolean = true;
  @Input() minPromo: boolean = false;

  // Event Emitters
  @Output() onSlideLeft = new EventEmitter<number>();
  @Output() onSlideRight = new EventEmitter<number>();
  @Output() onSwipeLeft = new EventEmitter<number>();
  @Output() onSwipeRight = new EventEmitter<number>();
  @Output() onFullscreenExit = new EventEmitter<boolean>();
  @Output() onIndexChanged = new EventEmitter<number>();
  @Output() onImageLazyLoad = new EventEmitter<ISlide>();
  @Output() onClick = new EventEmitter<{ slide: ISlide; index: number }>();
  @Output() onNavigate = new EventEmitter<any>();
  @Output() onBonusAcceptance = new EventEmitter<any>();
  @Output() onGameInfoOpen = new EventEmitter<any>();

  // Element References
  @ViewChild('container', { static: true }) container: ElementRef;
  @ViewChild('prevArrow', { static: true }) prevArrow: ElementRef;
  @ViewChild('nextArrow', { static: true }) nextArrow: ElementRef;

  get safeStyleDotColor(): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(`--dot-color: ${this.dotColor}`);
  }

  constructor(
    private slideshowService: SlideshowService,
    private renderer: Renderer2,
    private transferState: TransferState,
    private ngZone: NgZone,
    private cdRef: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
    private helpersService: HelpersService,
    private getDataService: GetDataService,
    @Inject(PLATFORM_ID) private platform_id: any,
    @Inject(DOCUMENT) private document: any,
  ) {}

  ngOnInit() {
    setTimeout(() => {
      if (this.isMobile) {
        if (this.imageUrls.length > 1) {
          this.imageUrls.forEach((element) => {
            this.slide(1);
          });
        }
      }
    }, 0);

    this.slideSub = this.slideshowService.slideEvent.subscribe((indexDirection: number) => {
      this.onSlide(indexDirection, true);
    });
    this.clickSub = this.slideshowService.clickEvent.subscribe(() => {
      this._onClick();
    });
    if (this.noLoop) {
      this.hideLeftArrow = true;
    }
  }

  ngAfterViewInit(): void {
    this.slideshowService.bind(this.container);
  }

  ngOnDestroy() {
    try {
      if (this.slideSub && !this.slideSub.closed) {
        this.slideSub.unsubscribe();
      }
    } catch (error) {
      console.warn('Slide Subscription error caught in ng-simple-slideshow OnDestroy:', error);
    }

    try {
      if (this.clickSub && !this.clickSub.closed) {
        this.clickSub.unsubscribe();
      }
    } catch (error) {
      console.warn('Click Subscription error caught in ng-simple-slideshow OnDestroy:', error);
    }

    try {
      this.slideshowService.unbind(this.container);
    } catch (error) {
      console.warn('Pointer Service unbind error caught in ng-simple-slideshow OnDestroy:', error);
    }

    try {
      if (this.autoplayIntervalId) {
        this.ngZone.runOutsideAngular(() => clearInterval(this.autoplayIntervalId));
        this.autoplayIntervalId = null;
      }
    } catch (error) {
      console.warn('Autoplay cancel error caught in ng-simple-slideshow OnDestroy:', error);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['imageUrls']) {
      this.slideIndex = 0;
    }
    if (changes['imageUrls'] && this.minPromo) {
      this.setFirstDotAsActiveForEachSlide();
    }

    if (changes['noLoop']) {
      if (changes['noLoop'].currentValue) {
        this.hideLeftArrow = this.slideIndex <= 0;
        this.hideRightArrow = this.slideIndex === this.slides.length - 1;
      } else {
        this.hideLeftArrow = false;
        this.hideRightArrow = false;
      }

      this.cdRef.detectChanges();
    }
  }

  ngDoCheck() {
    // if this is the first being called, create a copy of the input
    if (this.imageUrls && this.imageUrls.length > 0) {
      if (this.initial === true) {
        this.urlCache = Array.from(this.imageUrls);
      }

      if (this.isHidden === true) {
        this.renderer.removeStyle(this.container.nativeElement, 'display');
        this.isHidden = false;
      }

      this.setSlides();
    } else if (this.hideOnNoSlides === true) {
      this.renderer.setStyle(this.container.nativeElement, 'display', 'none');
      this.isHidden = true;
    }

    this.setStyles();
    this.handleAutoPlay();
    this.slideshowService.disableSwiping = this.disableSwiping;
    this.slideshowService.enableZoom = this.enableZoom;
    this.slideshowService.enablePan = this.enablePan;
  }

  /**
   * @param {number} indexDirection
   * @param {boolean} isSwipe
   * @description this is the function that should be called to make the slides change.
   *              indexDirection to move back is -1, to move forward is 1, and to stay in place is 0.
   *              0 is taken into account for failed swipes
   */

  public onSlide(indexDirection: number, isSwipe?: boolean): void {
    this.handleAutoPlay(this.stopAutoPlayOnSlide);
    this.slide(indexDirection, isSwipe);
  }

  /**
   * @description Redirect to current slide "href" if defined
   */
  private _onClick(): void {
    const currentSlide = this.slides.length > 0 && this.slides[this.slideIndex];
    this.onClick.emit({ slide: currentSlide, index: this.slideIndex });

    if (currentSlide && currentSlide.image.clickAction) {
      currentSlide.image.clickAction();
    } else if (currentSlide && currentSlide.image.href) {
      this.document.location.href = currentSlide.image.href;
    }
  }

  /**
   * @param {number} index
   * @description set the index to the desired index - 1 and simulate a right slide
   */
  goToSlide(index: number) {
    const beforeClickIndex = this.slideIndex;
    this.slideIndex = index - 1;

    this.setSlideIndex(1);

    if (this.slides[this.slideIndex] && !this.slides[this.slideIndex].loaded) {
      this.loadRemainingSlides();
    }

    this.handleAutoPlay(this.stopAutoPlayOnSlide);
    this.slideRight(beforeClickIndex);
    this.slides[beforeClickIndex].selected = false;
    this.slides[this.slideIndex].selected = true;

    this.cdRef.detectChanges();
  }

  /**
   * @param {number} index
   * @description set the index to the desired index - 1 and simulate a right slide
   */
  getSlideStyle(index: number) {
    const slide = this.slides[index];

    if (slide && slide.loaded) {
      return {
        'background-image': 'url(' + slide.image.url + ')',
        'background-size': slide.image.backgroundSize || this.backgroundSize,
        'background-position': slide.image.backgroundPosition || this.backgroundPosition,
        'background-repeat': slide.image.backgroundRepeat || this.backgroundRepeat,
        cursor: slide.image.cursor || 'default',
      };
    } else {
      // doesn't compile correctly if returning an empty object, sooooo.....
      return {
        'background-image': undefined,
        'background-size': undefined,
        'background-position': undefined,
        'background-repeat': undefined,
      };
    }
  }

  exitFullScreen(e: Event) {
    e.preventDefault();
    this.fullscreen = false;
    this.onFullscreenExit.emit(true);
  }

  /**
   * @param {number} indexDirection
   * @param {boolean} isSwipe
   * @description Set the new slide index, then make the transition happen.
   */
  private slide(indexDirection: number, isSwipe?: boolean): void {
    const oldIndex = this.slideIndex;

    if (this.slides[this.slideIndex].image.dots && this.minPromo) {
      this.setFirstDotAsActive(this.slides[this.slideIndex].image);
    }

    if (this.setSlideIndex(indexDirection)) {
      if (this.slides[this.slideIndex] && !this.slides[this.slideIndex].loaded) {
        this.loadRemainingSlides();
      }

      if (indexDirection === 1) {
        this.slideRight(oldIndex, isSwipe);
      } else {
        this.slideLeft(oldIndex, isSwipe);
      }

      this.slides[oldIndex].selected = false;
      this.slides[this.slideIndex].selected = true;
    }

    this.cdRef.detectChanges();
  }

  /**
   * @param {number} indexDirection
   * @description This is just treating the url array like a circular list.
   */
  private setSlideIndex(indexDirection: number): boolean {
    let willChange = true;
    this.slideIndex += indexDirection;

    if (this.noLoop) {
      this.hideRightArrow = this.slideIndex === this.slides.length - 1;
      this.hideLeftArrow = false;
    }

    if (this.slideIndex < 0) {
      if (this.noLoop) {
        this.slideIndex -= indexDirection;
        willChange = false;
        this.hideLeftArrow = true;
      } else {
        this.slideIndex = this.slides.length - 1;
      }
    } else if (this.slideIndex >= this.slides.length) {
      if (this.noLoop) {
        this.slideIndex -= indexDirection;
        willChange = false;
        this.hideRightArrow = true;
      } else {
        this.slideIndex = 0;
      }
    }

    if (willChange) {
      this.onIndexChanged.emit(this.slideIndex);
    }

    return willChange;
  }

  /**
   * @param {number} oldIndex
   * @param {boolean} isSwipe
   * @description This function handles the variables to move the CSS classes around accordingly.
   *              In order to correctly handle animations, the new slide as well as the slides to
   *              the left and right are assigned classes.
   */
  private slideLeft(oldIndex: number, isSwipe?: boolean): void {
    if (isSwipe === true) {
      this.onSwipeLeft.emit(this.slideIndex);
    } else {
      this.onSlideLeft.emit(this.slideIndex);
    }

    this.slides[this.getLeftSideIndex(oldIndex)].leftSide = false;
    this.slides[oldIndex].leftSide = true;
    this.slides[oldIndex].action = 'slideOutLeft';
    this.slides[this.slideIndex].rightSide = false;
    this.slides[this.getRightSideIndex()].rightSide = true;
    this.slides[this.slideIndex].action = 'slideInRight';
  }

  /**
   * @param {number} oldIndex
   * @param {boolean} isSwipe
   * @description This function handles the variables to move the CSS classes around accordingly.
   *              In order to correctly handle animations, the new slide as well as the slides to
   *              the left and right are assigned classes.
   */
  private slideRight(oldIndex: number, isSwipe?: boolean): void {
    if (isSwipe === true) {
      this.onSwipeRight.emit(this.slideIndex);
    } else {
      this.onSlideRight.emit(this.slideIndex);
    }

    this.slides[this.getRightSideIndex(oldIndex)].rightSide = false;
    this.slides[oldIndex].rightSide = true;
    this.slides[oldIndex].action = 'slideOutRight';
    this.slides[this.slideIndex].leftSide = false;
    this.slides[this.getLeftSideIndex()].leftSide = true;
    this.slides[this.slideIndex].action = 'slideInLeft';
  }

  /**
   * @description Check to make sure slide images have been set or haven't changed
   */
  private setSlides(): void {
    if (this.imageUrls) {
      if (this.checkCache() || this.initial === true) {
        this.initial = false;
        this.urlCache = Array.from(this.imageUrls);
        this.slides = [];

        if (this.lazyLoad === true) {
          this.buildLazyLoadSlideArray();
        } else {
          this.buildSlideArray();
        }
        this.cdRef.detectChanges();
      }
    }
  }

  /**
   * @description create the slides without background urls, which will be added in
   *              for the "lazy load," then load only the first slide
   */
  private buildLazyLoadSlideArray(): void {
    for (let image of this.imageUrls) {
      this.slides.push({
        image: typeof image === 'string' ? { url: null } : { url: null, href: image.href || '' },
        action: '',
        leftSide: false,
        rightSide: false,
        selected: false,
        loaded: false,
      });
    }
    if (this.slideIndex === -1) {
      this.slideIndex = 0;
    }
    this.slides[this.slideIndex].selected = true;
    this.loadFirstSlide();
    this.onIndexChanged.emit(this.slideIndex);
  }

  /**
   * @description create the slides with background urls all at once
   */
  private buildSlideArray(): void {
    for (let image of this.imageUrls) {
      this.slides.push({
        image: typeof image === 'string' ? { url: image } : image,
        action: '',
        leftSide: false,
        rightSide: false,
        selected: false,
        loaded: true,
      });
    }
    if (this.slideIndex === -1) {
      this.slideIndex = 0;
    }

    this.slides[this.slideIndex].selected = true;
    this.onIndexChanged.emit(this.slideIndex);
  }

  /**
   * @description load the first slide image if lazy loading
   *              this takes server side and browser side into account
   */
  private loadFirstSlide(): void {
    const tmpIndex = this.slideIndex;
    const tmpImage = this.imageUrls[tmpIndex];

    // if server side, we don't need to worry about the rest of the slides
    if (isPlatformServer(this.platform_id)) {
      this.slides[tmpIndex].image = typeof tmpImage === 'string' ? { url: tmpImage } : tmpImage;
      this.slides[tmpIndex].loaded = true;
      this.transferState.set(FIRST_SLIDE_KEY, this.slides[tmpIndex]);
    } else {
      const firstSlideFromTransferState = this.transferState.get(FIRST_SLIDE_KEY, null as any);
      // if the first slide didn't finish loading on the server side, we need to load it
      if (firstSlideFromTransferState === null) {
        let loadImage = new Image();
        loadImage.src = typeof tmpImage === 'string' ? tmpImage : tmpImage.url;
        loadImage.addEventListener('load', () => {
          this.slides[tmpIndex].image = typeof tmpImage === 'string' ? { url: tmpImage } : tmpImage;
          this.slides[tmpIndex].loaded = true;
          this.onImageLazyLoad.emit(this.slides[tmpIndex]);
          this.cdRef.detectChanges();
        });
      } else {
        this.slides[tmpIndex] = firstSlideFromTransferState;
        this.transferState.remove(FIRST_SLIDE_KEY);
      }
    }
  }

  /**
   * @description if lazy loading in browser, start loading remaining slides
   * @todo: figure out how to not show the spinner if images are loading fast enough
   */
  private loadRemainingSlides(): void {
    for (let i = 0; i < this.slides.length; i++) {
      if (!this.slides[i].loaded) {
        new Promise((resolve) => {
          const tmpImage = this.imageUrls[i];
          let loadImage = new Image();
          loadImage.addEventListener('load', () => {
            this.slides[i].image = typeof tmpImage === 'string' ? { url: tmpImage } : tmpImage;
            this.slides[i].loaded = true;
            this.cdRef.detectChanges();
            this.onImageLazyLoad.emit(this.slides[i]);
            // resolve();
          });
          loadImage.src = typeof tmpImage === 'string' ? tmpImage : tmpImage.url;
        });
      }
    }
  }

  /**
   * @param {boolean} stopAutoPlay
   * @description Start or stop autoPlay, don't do it at all server side
   */
  private handleAutoPlay(stopAutoPlay?: boolean): void {
    if (isPlatformServer(this.platform_id)) {
      return;
    }

    if (stopAutoPlay === true || this.autoPlay === false) {
      if (this.autoplayIntervalId) {
        this.ngZone.runOutsideAngular(() => clearInterval(this.autoplayIntervalId));
        this.autoplayIntervalId = null;
      }
    } else if (!this.autoplayIntervalId) {
      this.ngZone.runOutsideAngular(() => {
        this.autoplayIntervalId = setInterval(() => {
          if (
            !this.autoPlayWaitForLazyLoad ||
            (this.autoPlayWaitForLazyLoad &&
              this.slides[this.slideIndex] &&
              this.slides[this.slideIndex].loaded)
          ) {
            this.ngZone.run(() => this.slide(1));
          }
        }, this.autoPlayInterval);
      });
    }
  }

  /**
   * @description Keep the styles up to date with the input
   */
  private setStyles(): void {
    if (this.fullscreen) {
      this.renderer.setStyle(this.container.nativeElement, 'height', '100%');
      // Would be nice to make it configurable
      this.renderer.setStyle(this.container.nativeElement, 'background-color', 'white');
    } else {
      // Would be nice to make it configurable
      this.renderer.removeStyle(this.container.nativeElement, 'background-color');
      if (this.height) {
        this.renderer.setStyle(this.container.nativeElement, 'height', this.height);
      }

      if (this.minHeight) {
        this.renderer.setStyle(this.container.nativeElement, 'min-height', this.minHeight);
      }
    }
    if (this.arrowSize) {
      this.renderer.setStyle(this.prevArrow.nativeElement, 'height', this.arrowSize);
      this.renderer.setStyle(this.prevArrow.nativeElement, 'width', this.arrowSize);
      this.renderer.setStyle(this.nextArrow.nativeElement, 'height', this.arrowSize);
      this.renderer.setStyle(this.nextArrow.nativeElement, 'width', this.arrowSize);
    }
  }

  /**
   * @description compare image array to the cache, returns false if no changes
   */
  private checkCache(): boolean {
    return !(
      this.urlCache.length === this.imageUrls.length &&
      this.urlCache.every((cacheElement, i) => cacheElement === this.imageUrls[i])
    );
  }

  /**
   * @param {number} i
   * @returns {number}
   * @description get the index for the slide to the left of the new slide
   */
  private getLeftSideIndex(i?: number): number {
    if (i === undefined) {
      i = this.slideIndex;
    }

    if (--i < 0) {
      i = this.slides.length - 1;
    }

    return i;
  }

  /**
   * @param {number} i
   * @returns {number}
   * @description get the index for the slide to the right of the new slide
   */
  private getRightSideIndex(i?: number): number {
    if (i === undefined) {
      i = this.slideIndex;
    }

    if (++i >= this.slides.length) {
      i = 0;
    }

    return i;
  }

  /**
   * @param {number} index
   * @param {ISlide} slide
   * @returns {any}
   * @description a trackBy function for the ngFor loops
   */
  trackByFn(index: number, slide: ISlide): any {
    return slide.image;
  }

  /**
   * @description don't let click events fire, handle in pointer service instead
   */
  handleClick(event: any, slide: any) {
    const referral = this.getDataService.getReferralCode();
    const imageLink = slide.image.imgLink ? slide.image.imgLink : 'app/casino/my_casino';
    const url = `${this.environment.baseServerUrl}${this.environment.port}/${imageLink}${referral ? '?referral=' + referral : ''}`

    window.open(url, '_blank');
    event.preventDefault();
    event.stopPropagation();
  }

  public navigateByUrl(event: any , url: any) {

  }

  // Set active dot
  private setActiveDot(dotIndex: any, promoIndex: any, promotion: any) {
    for (let i = 0; i < this.slides[promoIndex].image.dots.length; i++) {
      let dotColor = i === dotIndex ? 'grey' : 'white';
      let dot = document.getElementById(
        'pagination-dot-left-menu-' + String(i) + String(promotion.id)
      );
      // @ts-ignore
      dot.style.background = dotColor;
    }
  }

  // Set first dot as active
  private setFirstDotAsActive(image: any) {
    if (!this.isMobile) {
      for (let i = 0; i < image.dots.length; i++) {
        let dotColor;
        let dot: any = document.getElementById(
          'pagination-dot-left-menu-' + String(i) + String(image.promotion['id'])
        );
        dotColor = i === 0 ? 'grey' : 'white';
        dot.style.background = dotColor;
      }
    }
  }

  // Set first dot as active for each slide
  private setFirstDotAsActiveForEachSlide() {
    setTimeout(() => {
      this.slides.forEach((slide) => {
        if (slide.image.dots) {
          this.setFirstDotAsActive(slide.image);
        }
      });
    }, 500);
  }

  // Click on any of dots in type games promotion
  public onDotSlide(dotIndex: any, promoIndex: any, promotion: any) {
    const slider: any = document.getElementById('slider-container-left-menu-' + String(promotion.id));
    if (!this.isMobile) {
      this.setActiveDot(dotIndex, promoIndex, promotion);
      slider.style.transform = 'translateX(-' + dotIndex * 100 + '%)';
    }
  }

  // Inform parrent for bonus acceptance
  public acceptBonus(event: any, promotion: any) {
    event.stopPropagation();
    this.onBonusAcceptance.emit(promotion);
  }

  // Inform parrent to open game info
  public openGameInfo(event: any, game: any) {
    event.stopPropagation();
    this.onGameInfoOpen.emit(game);
  }
}
