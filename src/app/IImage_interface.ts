export interface IImage {
  url: string;
  href?: string;
  clickAction?: Function;
  caption?: string;
  captionDescription?: string;
  title?: string;
  backgroundSize?: string;
  backgroundPosition?: string;
  backgroundRepeat?: string;
  buttonLink?: string;
  buttonColor?: string;
  buttonText?: string;
  games?: [];
  promotion: {
    id: number
    accepted?: boolean,
    buttonLabel: string,
  };
  dots?: any;
  cursor?: string;
  imgLink?: string;
  overlayImg?: string;
  slideNavigationTitle?: string;
}
