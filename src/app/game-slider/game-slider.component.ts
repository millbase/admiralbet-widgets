import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { environment } from 'src/environments/environment';
import { HelpersService } from "../services/helpers.service";
import { UtilService } from "../services/util.service";
import {GetDataService} from "../services/get-data.service";

declare var sideScroll: any;

@Component({
  selector: 'app-game-slider',
  templateUrl: './game-slider.component.html',
  styleUrls: ['./game-slider.component.scss'],
})
export class GameSliderComponent implements OnInit, OnChanges {
  @Input() location: any;
  @Input() games: any;
  @Input() pagination: boolean;
  @Output() gameInfoOpened = new EventEmitter<any>();
  private casinoMode: number;
  public environment = environment;
  public componentId = Math.random().toString(36).substring(2);
  public paginationDots = [];
  public currentDotIndex = 0;
  private allowScroll = true;
  public isMobile = this.helpersService.isMobile();
  public baseUrl = document.location.pathname;
  public backgroundJackpot = `url('${this.baseUrl}assets/icons/cro/jackpot.png')`;
  public backgroundTourn = `url('${this.baseUrl}/assets/icons/cro/turnir2.png')`;
  public backgroundHotIcon = `url('${this.baseUrl}/assets/images/heart-active.svg')`;

  constructor(
    private helpersService: HelpersService,
    private utilService: UtilService,
    private getDataService: GetDataService,
  ) {}

  ngOnInit(): void {
    if (!this.pagination) {
      this.setupCasinoByType();
      this.setGamesContainerWidth();
      this.setPaginationDotsLength();
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.pagination) {
      this.setupCasinoByType();
      this.setGamesContainerWidth();
      this.setPaginationDotsLength();
      this.currentDotIndex = 0;
    }
  }

  private setPaginationDotsLength() {
    if (this.games && this.games.length && !this.isMobile) {
      this.paginationDots.length = Math.ceil(this.games.length / 5);
      setTimeout(() => {
        if (this.paginationDots.length > 1) {
          this.setActiveDot(this.currentDotIndex);
        }
      }, 500);
    } else {
      this.paginationDots.length = 0;
    }
  }

  // Method for checking casino type
  private setupCasinoByType(): void {
    this.casinoMode = 0;
  }

  // side scroll
  public sideScroll(event: any, direction: string): void {
    event.stopPropagation();
    if (this.pagination) {
      if (this.allowScroll) {
        this.sideScrollHelper(this.componentId, direction, 10, 10);
      }

      if (direction === 'right' && this.allowScroll) {
        if (this.currentDotIndex + 1 === this.paginationDots.length) {
          this.currentDotIndex = this.paginationDots.length - 1;
          return;
        }
        this.currentDotIndex++;
        this.allowScroll = false;
      }

      if (direction === 'left' && this.allowScroll) {
        if (this.currentDotIndex === 0) {
          return;
        }
        this.currentDotIndex--;
        this.allowScroll = false;
      }

      this.setActiveDot(this.currentDotIndex);
      setTimeout(() => {
        this.allowScroll = true;
      }, 750);
    } else {
      this.sideScrollHelper(this.componentId, direction, 10, 10);
    }
  }
  private gameRowIntervalRef: any;
  sideScrollHelper(elementId: string, direction: string, speed: number, step: number) {
    window.clearInterval(this.gameRowIntervalRef);
    const element = document.getElementById(elementId);
    // @ts-ignore
    const distance = element.clientWidth;

    let scrollAmount = 0;
    this.gameRowIntervalRef = setInterval(() => {
      // @ts-ignore
      element.scrollLeft = direction === 'left' ? element.scrollLeft - step : element.scrollLeft + step;
      scrollAmount += step;
      if (scrollAmount >= distance) {
        window.clearInterval(this.gameRowIntervalRef);
      }
    }, speed);
  }

  // navigate to game info page
  public navigateToGameInfo(event: any, game: any): void {
    // const url = `${this.environment.baseServerUrl}/24sata/app/casino/my_casino/${game.providerName.toLowerCase()}/${game.code}`;
    const referral = this.getDataService.getReferralCode();
    let url = `${this.environment.baseServerUrl}${this.environment.port}/casino/${game.providerName.toLowerCase()}/${game.code}${referral ? '?referral=' + referral : ''}`;
    window.open(url, '_blank');
    event.stopPropagation();
  }

  public setGamesContainerWidth() {
    if (this.games && this.games.length && !this.isMobile && this.pagination) {
      setTimeout(() => {
        const gameWrapper = document.getElementsByClassName('gameImgWrapperSlider')[0];
        const gamesContainer = document.getElementById(this.componentId);
        // @ts-ignore
        gamesContainer.style.minWidth = (gameWrapper.clientWidth + 10) * 5 + 'px';
        // @ts-ignore
        gamesContainer.style.width = (gameWrapper.clientWidth + 10) * 5 + 'px';
      }, 0);
    }
  }

  private setActiveDot(index: number) {
    // @ts-ignore
    if (!this.isMobile && this.games.length > 5 && this.pagination) {
      for (let i = 0; i < this.paginationDots.length; i++) {
        let backgroundColor = i === index ? 'white' : 'transparent';
        setTimeout(() => {
          let paginationDot = document.getElementById(`pagination-dot-${this.componentId}${i}`);
          if (paginationDot) paginationDot.style.backgroundColor = backgroundColor;
        }, 100);
      }
    }
  }
}
