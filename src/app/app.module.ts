import {Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {createCustomElement} from "@angular/elements";
import { AdmiralWebComponentComponent } from './admiral-web-component/admiral-web-component.component';
import {HttpClientModule} from "@angular/common/http";
import {NewSlideshowComponent} from "./slideshow/slideshow.component";
import {GameSliderComponent} from "./game-slider/game-slider.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AdmiralWebComponentComponent,
    GameSliderComponent,
    NewSlideshowComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [HttpClientModule],
  entryComponents: [AdmiralWebComponentComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const el = createCustomElement(AdmiralWebComponentComponent, { injector });
    customElements.define('admiral-web-component', el);
  }

  ngDoBootstrap() {}
}
