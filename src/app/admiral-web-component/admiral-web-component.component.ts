import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import { GetDataService } from "../services/get-data.service";
import {HttpClientModule} from "@angular/common/http";
import { IImage } from "../IImage_interface";
import { HelpersService } from "../services/helpers.service";
import { PROMOTION_TYPE } from '../enums/promotion-type.enum';
import {environment} from "../../environments/environment";

@Component({
  selector: 'admiral-web-component',
  templateUrl: './admiral-web-component.component.html',
  styleUrls: ['./admiral-web-component.component.scss'],
  providers:[HttpClientModule, GetDataService, HelpersService]
})
export class AdmiralWebComponentComponent implements OnInit {
  @ViewChild('slideshow') slideshow: any;
  @ViewChild('slidesContainer', {static: true}) slidesContainer: any;

  public rawData: any;
  public imagesArray: IImage[] = [];
  public currentSlide = this.imagesArray[0];
  public currentSlideIndex = 0;
  public isMobile: boolean;
  public enableAutoplay = false;

  constructor(
    private getDataService: GetDataService,
    private helpersService: HelpersService
  ) { }

  ngOnInit(): void {
    this.isMobile = this.helpersService.isMobile();
    this.getDataService.dataForCarousel
        .subscribe((data: any) => {
          this.rawData = data;
          if(this.rawData.ads && this.rawData.ads.length > 0) {
            this.prepareData();
          }
    })

    const params = new URLSearchParams(window.location.search);
    const referral = params.get('referral');
    const tableID = params.get('id');
    if (referral) { this.getDataService.setReferralCode(referral); }
    if (tableID) { this.getDataService.setTableID(tableID); }

    this.getDataService.getCasinoGames();
  }

  prepareData() {
    this.rawData.ads.forEach((ad: any) => {
      let adButton, promotion, imgLink;

      // if ad is type REDIRECT, create btn for redirect
      if (ad.type === PROMOTION_TYPE.REDIRECT) {
        adButton = this.createAdButton(ad);
        imgLink = ad.referenceId;
      };
      // if ad is type BONUS set ad on image click for open promotion details
      if (ad.type === PROMOTION_TYPE.BONUS) {
        promotion = ad;
        imgLink = 'promotions/' + ad.id;
      };

      // if ad is type GAMES set link for redirection from referenceId
      if (ad.type === PROMOTION_TYPE.GAMES) {
        imgLink = ad.referenceId;
      };

      const imagePath = this.isMobile ? ad.wideImageMob : ad.wideImage;
      // Map data for IImage type for slideshow component
      this.imagesArray.push({
        url: `${environment.imagesBaseUrl}${imagePath}`,
        buttonLink: adButton?.link,
        buttonText: adButton?.label,
        games: ad.games,
        promotion: promotion,
        imgLink: imgLink,
        cursor: adButton?.link || promotion || imgLink ? 'pointer' : 'default',
        overlayImg: this.setOveralyImage(ad),
        slideNavigationTitle: ad.shortTitle || ad.code,
      });
    });
    this.currentSlide = this.imagesArray[this.currentSlideIndex];
  }


  // create ad button
  private createAdButton(ad: any): any {
    let adButton;
    if (ad.buttonLabel || ad.type === 'REDIRECT') {
      let link;
      switch (ad.type) {
        case PROMOTION_TYPE.REDIRECT:
          link = ad.referenceId;
          break;
      }
      adButton = { label: ad.buttonLabel, link };
    }
    return adButton;
  }

  // set overlay image
  private setOveralyImage(ad: any): string {
    if (this.isMobile) {
      return ad.overlayImageMob ? environment.imagesBaseUrl + ad.overlayImageMob : '';
    } else {
      return ad.overlayImage ? environment.imagesBaseUrl + ad.overlayImage : '';
    }
  }

  // tab navigate to slide
  public tabNavigateToSlide(slideIndex: number) {
    this.slideshow.goToSlide(slideIndex);
    this.currentSlideIndex = slideIndex;
    this.currentSlide = this.imagesArray[this.currentSlideIndex];
  }

  // swipe left on arrow click
  public onSwipeLeft(): void {
    this.slideshow.onSlide(-1);
  }

  // swipe right on arrow click
  public onSwipeRight(): void {
    this.slideshow.onSlide(1);
  }

  // on slide changes set current slide
  public slideChange(index: number): void {
    this.currentSlideIndex = index;
    this.currentSlide = this.imagesArray[index];
  }


  // Handle Img Click
  public handleImgLink() {
    window.open(`${this.currentSlide.imgLink}${environment.port}`, '_blank');
  }
}
