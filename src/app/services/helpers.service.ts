import { Injectable } from '@angular/core';

@Injectable()
export class HelpersService {
  // private isGameOpened = new BehaviorSubject<boolean>(false);
  // public gameSearch = new BehaviorSubject<string>(null);
  // public gameSearchResults = this.gameSearch.asObservable();

  constructor() {}

  // check for user agent string
  getUserAgent() {
    return navigator.userAgent;
  }

  // check if is android
  isAndroid() {
    return /Android/i.test(this.getUserAgent()) && !this.isWindows();
  }

  // check if is windows
  isWindows() {
    return /Windows Phone|IEMobile|WPDesktop/i.test(this.getUserAgent());
  }

  // check if is iPhone
  isIPhone() {
    return /iPhone/i.test(this.getUserAgent()) && !this.isIPad() && !this.isWindows();
  }

  // check if is iPod
  isIPod() {
    return /iPod/i.test(this.getUserAgent());
  }

  // check if is iPad
  // @ts-ignore
  isIPad() {
    // old method - don't work for new iPads and iPads Pro
    // return /iPad/i.test(this.getUserAgent());

    if (this.getIPadModel() !== 'Not an iPad') {
      return true;
    }
  }

  isTablet() {
    return /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(
      this.getUserAgent().toLowerCase()
    );
  }

  // iPad model checks.
  getIPadModel() {
    if (window.screen.height / window.screen.width === 1024 / 768) {
      // iPad, iPad 2, iPad Mini
      if (window.devicePixelRatio === 1) {
        return 'iPad 1, iPad 2, iPad Mini 1';
      }
      // iPad 3, 4, 5, Mini 2, Mini 3, Mini 4, Air, Air 2, Pro 9.7
      else {
        return 'iPad 3, iPad 4, iPad 5, iPad Air 1, iPad Air 2, iPad Mini 2, iPad Mini 3, iPad Mini 4, iPad Pro 9.7';
      }
    }
    // iPad Pro 10.5
    else if (window.screen.height / window.screen.width === 1112 / 834) {
      return 'iPad Pro 10.5';
    }
    // iPad Pro 12.9, Pro 12.9 (2nd Gen)
    else if (window.screen.height / window.screen.width === 1366 / 1024) {
      return 'iPad Pro 12.9, Pro 12.9 (2nd Gen)';
    }
    // Not an ipad
    else {
      return 'Not an iPad';
    }
  }

  // check if is ios
  iOS() {
    return this.isIPad() || this.isIPod() || this.isIPhone();
  }

  // check if is mobile
  isMobile() {
    return this.isAndroid() || this.isIPhone() || this.isIPad() || this.iOS();
  }

}
