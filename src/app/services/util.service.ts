import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class UtilService {
  // Function iterate throw object properties and every string trim to remove white spaces at the beginning and at the end
  public trimObjectProperties(obj: any): any {
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        if (typeof obj[prop] === 'string') {
          obj[prop] = obj[prop].trim();
        } else if (typeof obj[prop] === 'object' && obj[prop] !== null) {
          obj[prop] = this.trimObjectProperties(obj[prop]);
        }
      }
    }
    return obj;
  }

  public addBodyClass(classNames: any): void {
    if (typeof classNames === 'string') {
      document.body.classList.add(classNames);
    } else {
      document.body.classList.add(...classNames);
    }
  }
}
