import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable()
export class GetDataService {

  public dataForCarousel = new BehaviorSubject([]);
  public referral: string;
  public tableId: number;

  constructor(private http: HttpClient) { }
  getCasinoGames() {
    const requestOptions = {
      headers: {'Content-Language': 'sr', 'X-MBASE-TOKEN': 'null'} ,
    };

    // prod url
    const prodURL = `${environment.baseServerUrl}/mbase/api/casino/widget`;
    // stage url
    const stageURL = `${environment.baseServerUrl}:9009/mbase/api/casino/widget`;
    const url = environment.production ? prodURL : stageURL;

    const prodUrlWithID =`${environment.baseServerUrl}/mbase/api/casino/my-casino/${this.tableId}`;
    const stageUrlWithID =`${environment.baseServerUrl}:9009/mbase/api/casino/my-casino/${this.tableId}`;
    const urlWithId = environment.production ? prodUrlWithID : stageUrlWithID;

    const urlSwitch = this.tableId ? urlWithId : url;

    // const url = prodURL;
    const baseUrl = document.location.pathname.slice(0, document.location.pathname.length -1);
    // @ts-ignore

    this.http.get(urlSwitch, requestOptions).subscribe((data: any) => {
      data.ads = data.ads.map((ad: any) => {
        return Object.assign({}, ad, {
          image: ad.image ? `${baseUrl}${ad.image}` : null,
          imageMin: ad.imageMin ? `${baseUrl}${ad.imageMin}` : null,
          imageMinMob: ad.imageMinMob ? `${baseUrl}${ad.imageMinMob}` : null,
          imageMob: ad.imageMob ? `${baseUrl}${ad.imageMob}` : null,
          overlayImage: ad.overlayImage ? `${baseUrl}${ad.overlayImage}` : null,
          overlayImageMob: ad.overlayImageMob ? `${baseUrl}${ad.overlayImageMob}` : null,
          wideImage: ad.wideImage ? `${baseUrl}${ad.wideImage}` : null,
          wideImageMob: ad.wideImageMob ? `${baseUrl}${ad.wideImageMob}` : null,
          games: ad.games? ad.games.map((gameData: any) => {
            return Object.assign({}, gameData, {
              gameIcon: {location: gameData.gameIcon.location ? `${baseUrl}${gameData.gameIcon.location}` :  null},
              lowResGameIcon: {location: gameData.lowResGameIcon.location ? `${baseUrl}${gameData.lowResGameIcon.location}` : null},
              mobileGameIcon: {location: gameData.mobileGameIcon.location ? `${baseUrl}${gameData.mobileGameIcon.location}` : null},
            })
          }) : ''
        })
      })
      this.dataForCarousel.next(data);
    });
  }

  setReferralCode(code: string) {
    this.referral = code;
  }
  setTableID(tableId: string) {
    this.tableId = Number(tableId);
  }

  getReferralCode() {
    return this.referral;
  }
}
