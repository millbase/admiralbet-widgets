// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment_24sata.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const baseServerUrl = `https://mbase.mbet.rs`;
const wsBaseUrl = `wss://mbase.mbet.rs`;

export const environment = {
  production: false,
  baseServerUrl: baseServerUrl,
  apiUrl: `${baseServerUrl}/mbase`,
  port: ':9000',
  imagesBaseUrl: '', // because we need to make image assets exist on local server these param is empty
  nxcsDeposit: `${baseServerUrl}/nxcs-deposit`,
  birmanGamerUrl: `${baseServerUrl}/gt-casino`,
  trumpeterUrl: `${wsBaseUrl}/trumpeter`,
  corvus: `${baseServerUrl}/corvus`,
  corvusIban: `${baseServerUrl}/corvus-iban`,
  ernehost: `${baseServerUrl}`,
  ernehostIntesa: `${baseServerUrl}`,
  ernehostIPay: `${baseServerUrl}/lotto`,
  skrillDepositUrl: `${baseServerUrl}/skrill-deposit`,
  saferpayDepositUrl: `${baseServerUrl}/saferpay-deposit`,
  aircashDepositUrl: `${baseServerUrl}/aircash-deposit`,
  ernehostBankWithdrawal: `${baseServerUrl}/millennium-bank-withdrawal`,
  nxcsWithdrawal: `${baseServerUrl}/nxcs-withdrawal`,
  skrillWithdrawalUrl: `${baseServerUrl}/skrill-withdrawal`,
  aircashWithdrawalUrl: `${baseServerUrl}/aircash-withdrawal`,
  ibanWithdrawalUrl: `${baseServerUrl}/millennium-bank-withdrawal`,
  inOfficeWithdrawalUrl: `${baseServerUrl}/millennium-withdrawal`,
  apiImgIdPath: `${baseServerUrl}`,
  aBonDepositUrl: `${baseServerUrl}/abon-deposit`,
  fazi: `${baseServerUrl}/fazi`,
  psd2WithdrawalUrl: `${baseServerUrl}/raiffeisen-withdrawal`,
  gpCardDeposit: `${baseServerUrl}/gp-card`,
  additionalCrossOriginCommunication: false,
  onlineCasinoUrl: '',
  rollbar: {
    enabled: false,
    accessToken: '',
    captureUncaught: true,
    captureUnhandledRejections: true,
    environment: 'dev',
    host: 'localhost',
  },
  showReservedFounds: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
